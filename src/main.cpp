#include <Arduino.h>
#include <ESP8266WiFi.h>

int led = 2;
int btn = 3;

void setup()
{
    Serial.begin(9600);

    // Serial.begin(115200);
    int a = WiFi.scanNetworks();
    
    Serial.printf(" Number of networks: %d \n", a);

    for (int i = 0; i < a; i++)
    {
        Serial.printf("SSID: %s/%d/%s\n", WiFi.SSID(i).c_str(), WiFi.RSSI(i),  WiFi.BSSIDstr(i).c_str());
    }
    
}

void loop()
{
}